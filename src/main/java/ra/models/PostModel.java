package ra.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "title"
})
public class PostModel {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public PostModel() {
    }

    public PostModel(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public String toString() {
        return "PostModel{" +
                "id=" + id +
                ", title=" + title +
                '}';
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(title).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PostModel) == false) {
            return false;
        }
        PostModel rhs = ((PostModel) other);
        return new EqualsBuilder().append(id, rhs.id).append(title, rhs.title).isEquals();
    }

}
