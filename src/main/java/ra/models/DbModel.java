package ra.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "posts",
        "comments",
        "profile"
})
public class DbModel {

    @JsonProperty("posts")
    private List<PostModel> posts = null;
    @JsonProperty("comments")
    private List<CommentModel> comments = null;
    @JsonProperty("profile")
    private ProfileModel profile;

    /**
     * No args constructor for use in serialization
     */
    public DbModel() {
    }

    /**
     * @param posts
     * @param comments
     * @param profile
     */
    public DbModel(List<PostModel> posts, List<CommentModel> comments, ProfileModel profile) {
        super();
        this.posts = posts;
        this.comments = comments;
        this.profile = profile;
    }

    @JsonProperty("posts")
    public List<PostModel> getPosts() {
        return posts;
    }

    @JsonProperty("posts")
    public void setPosts(List<PostModel> posts) {
        this.posts = posts;
    }

    @JsonProperty("comments")
    public List<CommentModel> getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }

    @JsonProperty("profile")
    public ProfileModel getProfile() {
        return profile;
    }

    @JsonProperty("profile")
    public void setProfile(ProfileModel profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("posts", posts)
                .append("comments", comments)
                .append("profile", profile)
                .toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(posts)
                .append(comments)
                .append(profile)
                .toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DbModel) == false) {
            return false;
        }
        DbModel rhs = ((DbModel) other);
        return new EqualsBuilder()
                .append(posts, rhs.posts)
                .append(comments, rhs.comments)
                .append(profile, rhs.profile)
                .isEquals();
    }

}