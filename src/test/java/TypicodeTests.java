import ra.models.*;
import ra.controllers.TypicodeCtrl;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TypicodeTests {

    public TypicodeCtrl typicodeCtrl;

    @BeforeClass
    public void beforeClass(){
        typicodeCtrl = new TypicodeCtrl();
    }

    @Test
    public void getListOfPostsTest(){
        PostsModel testPosts = typicodeCtrl.getPosts();
        assertThat(testPosts.getPosts(), not(emptyCollectionOf(PostModel.class)));
    }

    @Test
    public void getPostTest(){
        Integer testPostId = 2;
        PostModel testPost = typicodeCtrl.getPost(testPostId);
        assertThat(testPost.getId(), is(testPostId));
    }

    @Test
    public void createPostTest(){
        PostModel testPostCreate = new PostModel(5, "Asdf");
        PostModel testPostResponse;

        Response response = typicodeCtrl.createPost(testPostCreate);
        testPostResponse = response.as(PostModel.class);

        assertThat(response.getStatusCode(), is(201));
        assertThat(testPostResponse.getId(), is(greaterThanOrEqualTo(4)));
    }

    @Test
    public void updatePostTest(){
        PostModel testPost = typicodeCtrl.getPost(2);

        PostModel testPostForUdpate = testPost;
        testPostForUdpate.setTitle(RandomStringUtils.randomAlphabetic(10));
        Response response = typicodeCtrl.updatePost(testPostForUdpate);

        assertThat(response.getStatusCode(), is(200));
    }

    @Test
    public void deletePostTest(){
        int testId = 2;

        Response response = typicodeCtrl.deletePost(testId);

        assertThat(response.getStatusCode(), is(200));
        assertThat(response.as(PostModel.class), is(new PostModel()));
    }

    @Test
    public void getListOfCommentsTest(){
        CommentsModel testComments = typicodeCtrl.getComments();
        assertThat(testComments.getComments(), not(emptyCollectionOf(CommentModel.class)));
    }

    @Test
    public void createCommentTest(){
        CommentModel testComment = new CommentModel(null, RandomStringUtils.randomAlphabetic(10), 5);

        Response response = typicodeCtrl.createComment(testComment);

        assertThat(response.getStatusCode(), is(201));
        assertThat(response.jsonPath().get("id"), is(not(nullValue())));
    }

    @Test
    public void getCommentByIdTest(){
        Integer testId = 2;

        CommentModel testComment = typicodeCtrl.getComment(testId);
        assertThat(testComment.getId(), is(testId));
    }

    @Test
    public void updateCommentTest(){
        Integer testId = 2;

        CommentModel testComment = typicodeCtrl.getComment(testId);
        testComment.setBody(RandomStringUtils.randomAlphabetic(10));

        Response response = typicodeCtrl.updateComment(testComment);
        assertThat(response.getStatusCode(), is(200));
    }

    @Test
    public void deleteCommentTest(){
        int testId = 2;

        Response response = typicodeCtrl.deleteComment(testId);

        assertThat(response.getStatusCode(), is(200));
        assertThat(response.as(CommentModel.class), is(new CommentModel()));
    }

    @Test
    public void getProfileTest(){
        ProfileModel testProfile = typicodeCtrl.getProfile();
        assertThat(testProfile.getName(), not(isEmptyOrNullString()));
    }

    @Test
    public void updateProfileTest(){
        ProfileModel testProfile = typicodeCtrl.getProfile();
        testProfile.setName(RandomStringUtils.randomAlphabetic(10));
        Response response = typicodeCtrl.updateProfile(testProfile);

        assertThat(response.getStatusCode(), is(200));
    }

    @Test
    public void getDbTest(){
        DbModel testDb = typicodeCtrl.getDb();
        assertThat(testDb.toString(), not(isEmptyOrNullString()));
    }

    @Test(groups = "validation")
    public void validateProfileTest(){
        Response response = typicodeCtrl.validateProfile();
        assertThat(response.asString(), matchesJsonSchemaInClasspath("./schemas/json/profile.json"));
    }

    @Test(groups = "validation")
    public void validatePostTest(){
        Response response = typicodeCtrl.validatePost();
        assertThat(response.asString(), matchesJsonSchemaInClasspath("./schemas/json/post.json"));
    }

    @Test(groups = "validation")
    public void validationCommentTest(){
        Response response = typicodeCtrl.validateComment();
        assertThat(response.asString(), matchesJsonSchemaInClasspath("./schemas/json/comment.json"));
    }

    @Test(groups = "validation")
    public void validationDbTest(){
        Response response = typicodeCtrl.validateDb();
        assertThat(response.asString(), matchesJsonSchemaInClasspath("./schemas/json/db.json"));
    }

}
